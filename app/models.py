from django.db import models


# Create your models here.
class Recipe(models.Model):
    title = models.CharField(max_length=200, blank=True, null=True)
    summary = models.CharField(max_length=200, blank=True, null=True)
    content = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.title or "? (no title) ?"


class Tag(models.Model):
    name = models.CharField(max_length=200, blank=True, null=True)

    # recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    def __str__(self):
        return self.name or "? (no title) ?"


class RecipeTag(models.Model):
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.recipe} / {self.tag}"


class Ingredient(models.Model):
    name_singular = models.CharField(max_length=200, blank=True, null=True)
    name_plural = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        if self.name_plural:
            return f"{self.name_singular} ({self.name_plural})" or "? (no title) ?"
        return f"{self.name_singular}" or "? (no title) ?"


class Unit(models.Model):
    name = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.name or "? (no title) ?"


class RecipeIngredientUnit(models.Model):
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    ingredient = models.ForeignKey(Ingredient, on_delete=models.CASCADE)
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE)
    value = models.FloatField(default=0)
    unit_is_display = models.BooleanField(default=False)

    def __str__(self):
        if self.unit_is_display:
            return f"{self.recipe} / {self.value} {self.unit} {self.ingredient}"
        return f"{self.recipe} / {self.value} {self.ingredient}"
