# Generated by Django 3.2.9 on 2021-12-07 14:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_recipeingredientunit'),
    ]

    operations = [
        migrations.AddField(
            model_name='recipe',
            name='nbPersonne',
            field=models.IntegerField(default=1),
        ),
    ]
