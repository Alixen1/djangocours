from django.forms import Form
from django import forms


class LoginForm(Form):
    username = forms.CharField(max_length=200)
    password = forms.CharField(max_length=200, widget=forms.PasswordInput)

    #def clean_email(self):
    #    username = self.cleaned_data['username']
    #    if not username.endswith("univ-amu.fr"):
    #        self.add_error('username', "*ONLY* univ-amu.fr")
    #    return username
